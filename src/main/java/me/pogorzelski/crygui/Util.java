package me.pogorzelski.crygui;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tools.ant.Main;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by pablo on 19.12.15.
 */
public class Util {

    public static final Logger logger = LogManager.getLogger(Logger.class.getName());
    private static final int BUFFER_SIZE = 4096;

    public static Boolean checkIntCon() {
        Socket sock = new Socket();
        InetSocketAddress addr = new InetSocketAddress("google.com", 80);
        try {
            sock.connect(addr, 3000);
            return true;
        } catch (IOException e) {
            return false;
        } finally {
            try {
                sock.close();
            } catch (IOException e) {
            }
        }
    }

//    public static void addToLog(TextArea textArea, String string) {
//
//        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//        Calendar cal = Calendar.getInstance();
//        Platform.runLater(new Runnable() {
//
//            public void run() {
//                textArea.appendText("\n" + dateFormat.format(cal.getTime()) + " : " + string);
//            }
//        });
//
//    }

    public static String combinePath(List<String> pathStrings) {
        File file = new File(".");
        for (String string : pathStrings) {
            file = new File(file.getAbsolutePath(), string);
        }
        return file.getAbsolutePath();
    }

    public static String getCurrentVersion() throws IOException {
        String downloadedVersion = "0.0.0";
        try {
            File changeLog = new File(combinePath(Arrays.asList(App.dataDir, App.binDir, "CHANGELOG.md")));
            FileInputStream fis = new FileInputStream(changeLog);
            byte[] data = new byte[(int) changeLog.length()];
            fis.read(data);
            fis.close();
            String changeLogContent = new String(data, "UTF-8");
//            logger.info(changeLogContent);
            Pattern linkPattern = Pattern.compile("(\\d\\.\\d\\.\\d*)");
            Matcher linkMatcher = linkPattern.matcher(changeLogContent);
            while (linkMatcher.find()) {
                downloadedVersion = linkMatcher.group(1);
                logger.info(downloadedVersion);
                break;
            }
        } catch (Exception exp) {
            return downloadedVersion;
        }
        return downloadedVersion;
    }

    public static void setCardRuneList() throws IOException {
        logger.info("Setting card list info");
        File cardList = new File(combinePath(Arrays.asList(App.dataDir, App.binDir, "supported.cards.txt")));
        BufferedReader cardListBr = new BufferedReader(new FileReader(cardList));
        String cardLine = cardListBr.readLine();
        while (cardLine != null) {
            if (!cardLine.startsWith("#")) {
                if(cardLine.contains("demon,")) {
                    MainCtrl.demonNames.add(cardLine.split(",")[0].trim());
                } else {
                    MainCtrl.cardNames.add(cardLine.split(",")[0].trim());
                }
            }
            cardLine = cardListBr.readLine();
        }

        File runeList = new File(combinePath(Arrays.asList(App.dataDir, App.binDir, "supported.runes.txt")));
        BufferedReader runeListBr = new BufferedReader(new FileReader(runeList));
        String runeLine = runeListBr.readLine();
        while (runeLine != null) {
            if (!runeLine.startsWith("#")) {
                MainCtrl.runeNames.add(runeLine.trim());
            }
            runeLine = runeListBr.readLine();
        }

        File skillList = new File(combinePath(Arrays.asList(App.dataDir, App.binDir, "supported.skills.txt")));
        BufferedReader skillListBr = new BufferedReader(new FileReader(skillList));
        String skillLine = skillListBr.readLine();
        while (skillLine != null) {
            if (!skillLine.startsWith("#")) {
                MainCtrl.skillNames.add(skillLine.split("\\|")[0].trim());
            }
            skillLine = skillListBr.readLine();
        }

    }

    public static void showAlert(String text, String header, String title, String type) {
        Alert alert= null;
        if (type.equals("info")) {
            alert= new Alert(Alert.AlertType.INFORMATION);
        } else if (type.equals("error")) {
            alert= new Alert(Alert.AlertType.ERROR);
        }
        alert.setContentText(text);
        alert.setHeaderText(header);
        alert.setTitle(title);
        alert.showAndWait();

    }

    public static String downloadCrystark(String currentVersion) throws IOException {
        logger.info("Trying to check for new release");
        URL url = new URL("https://bitbucket.org/Crystark/ek-battlesim/wiki/Home");
        Document doc = Jsoup.parse(url, 60 * 1000);
        logger.info("Parsing webpage");
        String downloadLink = doc.select("a[href*=deploy/downloads/]").attr("href").toString();
//        downloadLink = downloadLink.replace("/src/", "/raw/").replace("?at=releases", "");
        logger.info(downloadLink);
        String linkVersion = "0.0.0";
        Pattern linkPattern = Pattern.compile(".*-(\\d.\\d.\\d.*).zip");
        Matcher linkMatcher = linkPattern.matcher(downloadLink);
        logger.info(linkMatcher);
        while (linkMatcher.find()) {
            linkVersion = linkMatcher.group(1);
        }
        logger.info("compare");
        logger.info(currentVersion);
        logger.info(linkVersion);
        if (!currentVersion.equals(linkVersion) && !linkVersion.equals("0.0.0")) {
            Alert alertUpdate = new Alert(Alert.AlertType.INFORMATION);
            alertUpdate.setContentText("Downloading new version of Crystark " + linkVersion + ". \n Click OK and please wait.");
            alertUpdate.setHeaderText("Updating");
            alertUpdate.setTitle("Update information");
            alertUpdate.showAndWait();
            URL link = new URL(downloadLink);
            InputStream in = new BufferedInputStream(link.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = 0;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();

            File fileName = new File(App.dataDir, "binary.zip");
            FileOutputStream fos = new FileOutputStream(fileName);
            fos.write(response);
            fos.close();

            unzip(fileName, new File(App.dataDir));
            Files.copy(new File(new File(App.dataDir, App.binDir), "ek-battlesim.exe").toPath(), new File(new File(App.dataDir, App.binDir), "ek-battlesim.jar").toPath(), StandardCopyOption.REPLACE_EXISTING);
            alertUpdate.close();
            logger.info("Updated crystark");
        }
        return linkVersion;
    }

    public static void unzip(File zipFilePath, File destDirectory) throws IOException {
        File destDir = destDirectory;
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractFile(zipIn, filePath);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }

    private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }


    public static void addToLog(final TextArea textArea, final String string) {

        final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        final Calendar cal = Calendar.getInstance();
        Platform.runLater(new Runnable() {

            public void run() {
                textArea.appendText("\n" + dateFormat.format(cal.getTime()) + " : " + string);
            }
        });

    }
}

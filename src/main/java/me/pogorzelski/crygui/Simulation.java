package me.pogorzelski.crygui;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by pablo on 20.12.15.
 */
public class Simulation {
    public static final Logger logger = LogManager.getLogger(Logger.class.getName());


    public List<Card> cards = new ArrayList<Card>();
    public List<Rune> runes = new ArrayList<Rune>();
    public String target;
    public String demonName;
    public Integer iterations;
    public Integer playerLvl;


    public void dumpDeck() throws IOException {
        File outputFile = new File(Util.combinePath(Arrays.asList(App.dataDir, App.binDir, App.tmpDeckFile)));
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
        StringBuilder builder = new StringBuilder();
        builder.append("Player level:").append(playerLvl).append(System.getProperty("line.separator"));
        for (Card card : cards) {
            builder.append(card.getName()).append(",");
            if (card.getEvoLvl() != null) {
                if (card.getEnchantLvl() == card.getEvoLvl() - 10) {
                    builder.append(card.getEnchantLvl());
                } else {
                    builder.append(card.getEnchantLvl()).append("+").append(card.getEvoLvl());
                }
            } else {
                builder.append(card.getEnchantLvl());
            }
            if (card.getEvoSkill() != null) {
                builder.append(",");
                String evoSkill = card.getEvoSkill().trim();
                if (evoSkill.startsWith("Desperat")) {
                    Integer firstChar = evoSkill.indexOf(":");
                    evoSkill = new StringBuilder(evoSkill).replace(0, firstChar + 1, "").toString();
                    evoSkill = evoSkill.trim();
                    evoSkill = "D_" + evoSkill;
                }
                if (evoSkill.startsWith("Quick Strike")) {
                    Integer firstChar = evoSkill.indexOf(":");
                    evoSkill = new StringBuilder(evoSkill).replace(0, firstChar + 1, "").toString();
                    evoSkill = evoSkill.trim();
                    evoSkill = "QS_" + evoSkill;
                }
//                if (evoSkill.substring(evoSkill.length()-1).matches("^-?\\d+$")) {
//                    Integer lastSpace = evoSkill.lastIndexOf(" ");
//                    evoSkill = new StringBuilder(evoSkill).replace(lastSpace, lastSpace+1,":").toString();
//                }
                logger.info(evoSkill);
                builder.append(evoSkill);
            }
            builder.append(System.getProperty("line.separator"));
        }
        builder.append(System.getProperty("line.separator"));
        for (Rune rune : runes) {
            builder.append(rune.getName());
//                    .append(":L");
//            builder.append(rune.getEnchantLvl())
            builder.append(System.getProperty("line.separator"));
        }
        writer.write(builder.toString());
        writer.close();

    }

    public String run() throws IOException {
        List<String> options = new ArrayList<String>();
        options.add("java");
        options.add("-jar");
        options.add("ek-battlesim.jar");
        options.add("-npb");
        Integer cores = Runtime.getRuntime().availableProcessors() * 2 * 3;
        // options.add("-th");
        // options.add(cores.toString());
        options.add("-i");
        options.add(String.valueOf(iterations));
        options.add("-dk");
        options.add(App.tmpDeckFile);
        if (target.equals("Demon Invasion")) {
            options.add("-dm");
            options.add(demonName);
        } else if (target.equals("Kingdom War")) {
            options.add("-kw");
        } else if (target.equals("Thief")) {
            options.add("-thief");
        }
        ProcessBuilder processBuilder = new ProcessBuilder(options);
        processBuilder.directory(new File(App.dataDir, App.binDir));
        Process simProcess = processBuilder.start();
        BufferedReader br = new BufferedReader(new InputStreamReader(simProcess.getInputStream()));
        String line = null;
        final StringBuilder builder = new StringBuilder();
        while ((line = br.readLine()) != null) {
            builder.append(line).append(System.getProperty("line.separator"));
        }
        simProcess.destroy();
        return builder.toString();
    }
}

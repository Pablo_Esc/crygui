package me.pogorzelski.crygui;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by pablo on 19.12.15.
 */
public class Card {
    private final SimpleStringProperty name = new SimpleStringProperty("");
    private final SimpleIntegerProperty enchantLvl = new SimpleIntegerProperty(0);
    private final SimpleIntegerProperty evoLvl = new SimpleIntegerProperty(0);
    private final SimpleStringProperty evoSkill = new SimpleStringProperty("");


    public String getName() {
        return name.get();
    }

    public void setName(String fName) {
        name.set(fName);
    }


    public Integer getEnchantLvl() {
        return enchantLvl.get();
    }

    public void setEnchantLvl(Integer fEnchantLvl) {
        enchantLvl.set(fEnchantLvl);
    }

    public Integer getEvoLvl() {
        return evoLvl.get();
    }

    public void setEvoLvl(Integer fEvoLvl) {
        evoLvl.set(fEvoLvl);
    }

    public String getEvoSkill() {
        return evoSkill.get();
    }

    public void setEvoSkill(String fEvoSkill) {
        evoSkill.set(fEvoSkill);
    }

}

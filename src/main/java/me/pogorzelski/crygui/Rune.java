package me.pogorzelski.crygui;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by pablo on 20.12.15.
 */
public class Rune {
    private final SimpleStringProperty name = new SimpleStringProperty("");
    private final SimpleIntegerProperty enchantLvl = new SimpleIntegerProperty(0);


    public String getName() {
        return name.get();
    }

    public void setName(String fName) {
        name.set(fName);
    }


    public Integer getEnchantLvl() {
        return enchantLvl.get();
    }

    public void setEnchantLvl(Integer fEnchantLvl) {
        enchantLvl.set(fEnchantLvl);
    }

}

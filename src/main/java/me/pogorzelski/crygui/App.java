package me.pogorzelski.crygui;

import java.io.File;
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.apache.commons.configuration.PropertiesConfiguration;

public class App extends Application {
	public static Stage currentStage;
	public static final String dataDir = "data";
	public static final String binDir = "ek-battlesim";
	public static final String arenaDir = "ranked";
	public static final String tmpDeckFile = "tmp_deck.txt";
	public static final String tmpOutputFile = "tmp_output.txt";
	public static final String tmpInputFile = "tmp_input.txt";
    public static PropertiesConfiguration propertiesHandle = null;
	public static void main(String[] args) throws IOException {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {

		Font.loadFont(getClass().getResourceAsStream("/fonts/Lato-Reg.ttf"), 12);
		Font.loadFont(getClass().getResourceAsStream("/fonts/Lato-Bol.ttf"), 12);
		File propertiesFile = new File("crygui.properties");
		if (!propertiesFile.exists() || !propertiesFile.isFile())
			propertiesFile.createNewFile();
		propertiesHandle = new PropertiesConfiguration("crygui.properties");
        File dataDir = new File(this.dataDir);
        if (!dataDir.exists() || !dataDir.isDirectory())
            dataDir.mkdir();

		Parent root = FXMLLoader.load(getClass().getResource("/fxml/main.fxml"));
		Scene scene = new Scene(root);
		scene.getStylesheets().add("styles/main.css");
		stage.setTitle("Crystark sim GUI");
		stage.setScene(scene);
		App.currentStage = stage;
		stage.show();
	}
}

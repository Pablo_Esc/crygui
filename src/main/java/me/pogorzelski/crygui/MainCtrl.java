package me.pogorzelski.crygui;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.textfield.TextFields;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;


public class MainCtrl implements Initializable {

    public static final Logger logger = LogManager.getLogger(Logger.class.getName());


    public TableView<Card> tableViewCards;
    public Tab tabPaneMain;
    public TableView<Rune> tableViewRunes;
    public TextField textFieldCardEvoSkill;
    public TextField textFieldCardName;
    public TextField textFieldRuneName;
    public Card selectedCard;
    public Rune selectedRune;

    public static List<String> cardNames = new ArrayList<String>();
    public static List<String> runeNames = new ArrayList<String>();
    public static List<String> skillNames = new ArrayList<String>();
    public static List<String> demonNames = new ArrayList<String>();
    public ComboBox comboBoxCardEnchantLvl;
    public ComboBox comboBoxCardEvoLvl;
    public ComboBox comboBoxRuneLvl;
    public ObservableList<Card> cardObservableList = FXCollections.observableArrayList();
    public ObservableList<Rune> runeObservableList = FXCollections.observableArrayList();
    public Button buttonAddCard;
    public Button buttonAddRune;
    public Button buttonUpdateCard;
    public Button buttonDeleteCard;
    public Button buttonUpdateRune;
    public Button buttonDeleteRune;
    public HBox targetHBox;
    public ComboBox comboBoxTargetList;
    public ComboBox comboBoxDemonList;
    public TextArea textAreaLog;
    public Tab tabPaneSettings;
    public Tab tabPaneOutput;
    public Button buttonRunSim;
    public TextArea textAreaOutput;
    public TabPane tabPane;
    public TextField textFieldPlayerLevel;
    public TextField textFieldIterations;
    public Label labelSimStatus;
    public Tab tabPaneCards;
    public Tab tabPaneRunes;

    public void initControls() {
        textAreaLog.setPrefRowCount(5);
        textAreaOutput.setPrefRowCount(15);
        tableViewCards.setFixedCellSize(25);
        String playerLvl = (String) App.propertiesHandle.getProperty("player-lvl");
        if (playerLvl == null)
            playerLvl = "100";
        textFieldPlayerLevel.setText(playerLvl);
        String iterations = (String) App.propertiesHandle.getProperty("iterations");
        if (iterations == null)
            iterations = "50000";
        textFieldIterations.setText(iterations);

        tableViewCards.prefHeightProperty().set(10 * tableViewCards.getFixedCellSize() + 30);
        tableViewCards.minHeightProperty().set(10 * tableViewCards.getFixedCellSize() + 30);
        buttonDeleteCard.disableProperty().set(true);
        buttonUpdateCard.disableProperty().set(true);
        tableViewCards.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Card>() {
            public void changed(ObservableValue<? extends Card> observable, Card oldValue, Card newValue) {
                if (newValue != null) {
                    selectedCard = newValue;
                    textFieldCardName.setText(newValue.getName());
                    textFieldCardEvoSkill.setText(newValue.getEvoSkill());
                    comboBoxCardEnchantLvl.getSelectionModel().select(newValue.getEnchantLvl());
                    comboBoxCardEvoLvl.getSelectionModel().select(newValue.getEvoLvl());
                    buttonDeleteCard.disableProperty().set(false);
                    buttonUpdateCard.disableProperty().set(false);
                }
            }
        });

        cardObservableList.addListener(new ListChangeListener<Card>() {
            public void onChanged(Change<? extends Card> c) {
                if (cardObservableList.size() == 10) {
                    buttonAddCard.disableProperty().set(true);
                } else {
                    buttonAddCard.disableProperty().set(false);
                }
                if (cardObservableList.size() == 0) {
                    buttonDeleteCard.disableProperty().set(true);
                    buttonUpdateCard.disableProperty().set(true);
                }
            }
        });


        tableViewRunes.setFixedCellSize(25);
        tableViewRunes.prefHeightProperty().set(4 * tableViewRunes.getFixedCellSize() + 30);
        tableViewRunes.minHeightProperty().set(4 * tableViewRunes.getFixedCellSize() + 30);
        buttonDeleteRune.disableProperty().set(true);
        buttonUpdateRune.disableProperty().set(true);

        tableViewRunes.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Rune>() {
            public void changed(ObservableValue<? extends Rune> observable, Rune oldValue, Rune newValue) {
                if (newValue != null ) {
                    selectedRune = newValue;
                    textFieldRuneName.setText(newValue.getName());
//                comboBoxRuneLvl.getSelectionModel().select(newValue.getEnchantLvl());
                    buttonDeleteRune.disableProperty().set(false);
                    buttonUpdateRune.disableProperty().set(false);

                }
            }
        });

        runeObservableList.addListener(new ListChangeListener<Rune>() {
            public void onChanged(Change<? extends Rune> c) {
                if (runeObservableList.size() == 4) {
                    buttonAddRune.disableProperty().set(true);
                } else {
                    buttonAddRune.disableProperty().set(false);
                }
                if (runeObservableList.size() == 0) {
                    buttonDeleteRune.disableProperty().set(true);
                    buttonUpdateRune.disableProperty().set(true);
                }

            }
        });


        //add data to combobox'es
        for (int i = 0; i <= 15; i++) {
            comboBoxCardEnchantLvl.getItems().add(15 - i);
        }
        for (int i = 0; i <= 5; i++) {
            comboBoxCardEvoLvl.getItems().add(5 - i);
//            comboBoxRuneLvl.getItems().add(5 - i);
        }

        List<String> targetList = Arrays.asList("Demon Invasion", "Kingdom War", "Thief");
        comboBoxTargetList.getItems().addAll(targetList);
        comboBoxDemonList.getItems().addAll(demonNames);
        comboBoxDemonList.visibleProperty().set(false);
        comboBoxTargetList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if (newValue.equals("Demon Invasion")) {
                    comboBoxDemonList.visibleProperty().set(true);
                } else {
                    comboBoxDemonList.visibleProperty().set(false);
                }
            }
        });


        tableViewCards.setItems(cardObservableList);
        tableViewRunes.setItems(runeObservableList);

        File dataFile = new File(App.dataDir);
        if (!dataFile.exists() || !dataFile.isDirectory()) {
            dataFile.mkdirs();
        }

    }

    public void initialize(URL arg0, ResourceBundle arg1) {
        Util.addToLog(textAreaLog, "Initalizing crystark gui");
        try {
            String currentVersion;
            currentVersion = Util.getCurrentVersion();
            Util.downloadCrystark(currentVersion);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Util.addToLog(textAreaLog, "Loading game data");
        try {
            Util.setCardRuneList();
        } catch (IOException e) {
            e.printStackTrace();
        }
        initControls();

        TextFields.bindAutoCompletion(textFieldCardName, cardNames);
        TextFields.bindAutoCompletion(textFieldRuneName, runeNames);
        TextFields.bindAutoCompletion(textFieldCardEvoSkill, skillNames);
    }


    public void addCard(ActionEvent actionEvent) {
        Boolean isValid = true;
        Card card = new Card();
        if (textFieldCardName.getText().length() > 0 && cardNames.contains(textFieldCardName.getText())) {
            card.setName(textFieldCardName.getText());
        } else {
            isValid = false;
            Util.showAlert(textFieldCardName.getText() + " is not a proper card name", "Error", "Error", "error");
            return;
        }
        if (!comboBoxCardEnchantLvl.getSelectionModel().isEmpty()) {
            card.setEnchantLvl((Integer) comboBoxCardEnchantLvl.getSelectionModel().getSelectedItem());
        } else {
            card.setEnchantLvl(10);
        }
        if (!comboBoxCardEvoLvl.getSelectionModel().isEmpty()) {
            card.setEvoLvl((Integer) comboBoxCardEvoLvl.getSelectionModel().getSelectedItem());
        } else {
            if (card.getEnchantLvl() >= 10) {
                card.setEvoLvl(card.getEnchantLvl() - 10);
            }
        }

        if (card.getEvoLvl() < card.getEnchantLvl() - 10) {
            isValid = false;
            Util.showAlert(textFieldCardName.getText() + " can't have evo lower then enchant", "Error", "Error", "error");
            return;
        }

        if (textFieldCardEvoSkill.getText().length() == 0 || skillNames.contains(textFieldCardEvoSkill.getText())) {
            card.setEvoSkill(textFieldCardEvoSkill.getText());
        } else {
            isValid = false;
            Util.showAlert(textFieldCardEvoSkill.getText() + " is not a proper skill name", "Error", "Error", "error");
            return;
        }
        if (isValid)
            cardObservableList.add(card);
        logger.info("Card added");
    }

    public void resetCardData(ActionEvent actionEvent) {
        textFieldCardEvoSkill.clear();
        textFieldCardName.clear();
        comboBoxCardEvoLvl.getSelectionModel().clearSelection();
        comboBoxCardEnchantLvl.getSelectionModel().clearSelection();
    }

    public void updateCard(ActionEvent actionEvent) {
        logger.info("we've got selected card : " + selectedCard.getName());
        if (textFieldCardName.getText().length() > 0 && cardNames.contains(textFieldCardName.getText())) {
            selectedCard.setName(textFieldCardName.getText());
        } else {
            Util.showAlert(textFieldCardName.getText() + " is not a proper card name", "Error", "Error", "error");
            return;
        }
        if (!comboBoxCardEnchantLvl.getSelectionModel().isEmpty()) {
            selectedCard.setEnchantLvl((Integer) comboBoxCardEnchantLvl.getSelectionModel().getSelectedItem());
        } else {
            selectedCard.setEnchantLvl(10);
        }
        if (!comboBoxCardEvoLvl.getSelectionModel().isEmpty()) {
            selectedCard.setEvoLvl((Integer) comboBoxCardEvoLvl.getSelectionModel().getSelectedItem());
        } else {
            if (selectedCard.getEnchantLvl() >= 10) {
                selectedCard.setEvoLvl(selectedCard.getEnchantLvl() - 10);
            }
        }
        logger.info(selectedCard.getEvoLvl());
        logger.info(selectedCard.getEnchantLvl());
        if (selectedCard.getEvoLvl() < selectedCard.getEnchantLvl() - 10) {
            Util.showAlert(textFieldCardName.getText() + " can't have evo lower then enchant", "Error", "Error", "error");
            return;
        }

        if (textFieldCardEvoSkill.getText().length() == 0 || skillNames.contains(textFieldCardEvoSkill.getText())) {
            selectedCard.setEvoSkill(textFieldCardEvoSkill.getText());
        } else {
            Util.showAlert(textFieldCardEvoSkill.getText() + " is not a proper skill name", "Error", "Error", "error");
            return;
        }
        //refresh
        tableViewCards.getColumns().get(0).setVisible(false);
        tableViewCards.getColumns().get(0).setVisible(true);
    }

    public void deleteCard(ActionEvent actionEvent) {
        if (selectedCard != null)
            cardObservableList.remove(selectedCard);
    }

    public void addRune(ActionEvent actionEvent) {
        Boolean isValid = true;
        Rune rune = new Rune();
        if (textFieldRuneName.getText().length() > 0 && runeNames.contains(textFieldRuneName.getText())) {
            rune.setName(textFieldRuneName.getText());
        } else {
            isValid = false;
            Util.showAlert(textFieldRuneName.getText() + " is not a proper rune name", "Error", "Error", "error");
            return;
        }
//        if (!comboBoxRuneLvl.getSelectionModel().isEmpty()) {
//            rune.setEnchantLvl((Integer) comboBoxRuneLvl.getSelectionModel().getSelectedItem());
//        } else {
//            rune.setEnchantLvl(0);
//        }

        for (Rune addedRune : runeObservableList) {
            if (addedRune.getName().equals(rune.getName())) {
                isValid = false;
                Util.showAlert(textFieldRuneName.getText() + " can't be placed more then once", "Error", "Error", "error");
                return;
            }
        }

        if (isValid)
            runeObservableList.add(rune);
        logger.info("Rune added");
    }

    public void resetRuneData(ActionEvent actionEvent) {
        textFieldRuneName.clear();
        comboBoxRuneLvl.getSelectionModel().clearSelection();
    }

    public void deleteRune(ActionEvent actionEvent) {
        if (selectedRune != null)
            runeObservableList.remove(selectedRune);
    }

    public void updateRune(ActionEvent actionEvent) {
        logger.info("we've got selected card : " + selectedRune.getName());

        if (textFieldRuneName.getText().length() > 0 && runeNames.contains(textFieldRuneName.getText())) {

            for (Rune addedRune : runeObservableList) {
                if (addedRune.getName().equals(textFieldRuneName.getText())) {
                    Util.showAlert(textFieldRuneName.getText() + " can't be placed more then once", "Error", "Error", "error");
                    return;
                }
            }

            selectedRune.setName(textFieldRuneName.getText());
        } else {
            Util.showAlert(textFieldRuneName.getText() + " is not a proper rune name", "Error", "Error", "error");
            return;
        }

        //refresh
        tableViewRunes.getColumns().get(0).setVisible(false);
        tableViewRunes.getColumns().get(0).setVisible(true);
    }

    public void runSim(ActionEvent actionEvent) throws IOException {
        if (comboBoxTargetList.getSelectionModel().getSelectedItem() == null ||
                (comboBoxTargetList.getSelectionModel().getSelectedItem() == "Demon Invasion" && comboBoxDemonList.getSelectionModel().getSelectedItem() == null)) {
            Util.showAlert("You must select sim target and options", "Error", "Error", "error");
            return;
        }
        tabPaneMain.disableProperty().set(true);
        tabPaneSettings.disableProperty().set(true);
        tabPaneCards.disableProperty().set(true);
        tabPaneRunes.disableProperty().set(true);
        buttonRunSim.disableProperty().set(true);
        textAreaOutput.clear();
        SingleSelectionModel<Tab> singleSelectionModel = tabPane.getSelectionModel();
        singleSelectionModel.select(tabPaneOutput);
        simService.reset();
        simService.start();
    }

    public void deckSave(ActionEvent actionEvent) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save deck");
        File outputFile = fileChooser.showSaveDialog(App.currentStage);
        if (outputFile != null) {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
            StringBuilder builder = new StringBuilder();
            for (Card card : cardObservableList) {
                builder.append("C").append(",");
                builder.append(card.getName()).append(",");
                builder.append(card.getEnchantLvl()).append(",");
                builder.append(card.getEvoLvl()).append(",");
                builder.append(card.getEvoSkill()).append(",").append(System.getProperty("line.separator"));
            }
            for (Rune rune : runeObservableList) {
                builder.append("R").append(",");
                builder.append(rune.getName()).append(",");
                builder.append(rune.getEnchantLvl()).append(",").append(System.getProperty("line.separator"));
            }
            writer.write(builder.toString());
            writer.close();
            Util.addToLog(textAreaLog, "Deck saved");
        }
    }

    public void deckLoad(ActionEvent actionEvent) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Deck");
        File outputFile = fileChooser.showOpenDialog(App.currentStage);
        if (outputFile != null) {
            BufferedReader reader = new BufferedReader(new FileReader(outputFile));
            String line = "";
            runeObservableList.clear();
            cardObservableList.clear();
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("C")) {
                    Card card = new Card();
                    card.setName(line.split(",")[1]);
                    card.setEnchantLvl(Integer.valueOf(line.split(",")[2]));
                    card.setEvoLvl(Integer.valueOf(line.split(",")[3]));
                    if (line.split(",").length > 4)
                        card.setEvoSkill(line.split(",")[4]);
                    cardObservableList.add(card);
                }
                if (line.startsWith("R")) {
                    Rune rune = new Rune();
                    rune.setName(line.split(",")[1]);
                    rune.setEnchantLvl(Integer.valueOf(line.split(",")[2]));
                    runeObservableList.add(rune);
                }
            }
            reader.close();
            Util.addToLog(textAreaLog, "Deck loaded");
        }
    }

    public void deckReset(ActionEvent actionEvent) {
        runeObservableList.clear();
        cardObservableList.clear();
    }


    Service<Integer> simService = new Service<Integer>() {
        @Override
        protected void failed() {
            super.failed();
            labelSimStatus.setText("Sim status : Failed");
        }

        @Override
        protected void running() {
            super.running();
            labelSimStatus.setText("Sim status : Running");
        }

        @Override
        protected void succeeded() {
            super.succeeded();
            labelSimStatus.setText("Sim status : Complete");
            Util.addToLog(textAreaLog, "Sim ended with success");
            tabPaneMain.disableProperty().set(false);
            tabPaneSettings.disableProperty().set(false);
            tabPaneCards.disableProperty().set(false);
            tabPaneRunes.disableProperty().set(false);
            buttonRunSim.disableProperty().set(false);
        }


        protected Task<Integer> createTask() {
            return new Task<Integer>() {
                @Override
                protected Integer call() throws Exception {
                    Util.addToLog(textAreaLog, "Sim starting");
                    Simulation simulation = new Simulation();
                    simulation.cards.addAll(cardObservableList);
                    simulation.runes.addAll(runeObservableList);
                    simulation.iterations = Integer.valueOf(textFieldIterations.getText());
                    simulation.playerLvl = Integer.valueOf(textFieldPlayerLevel.getText());
                    simulation.target = (String) comboBoxTargetList.getSelectionModel().getSelectedItem();
                    if (simulation.target != null && simulation.target.equals("Demon Invasion")) {
                        simulation.demonName = (String) comboBoxDemonList.getSelectionModel().getSelectedItem();
                    }
                    simulation.dumpDeck();
                    String simOutput = simulation.run();
                    textAreaOutput.setText(simOutput);
                    if (Thread.currentThread().isInterrupted()) {
                        Thread.currentThread().interrupt();
                        return null;
                    }
                    return null;
                }
            };
        }
    };

    public void outputSave(ActionEvent actionEvent) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save deck");
        File outputFile = fileChooser.showSaveDialog(App.currentStage);
        if (outputFile != null) {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
            writer.write(textAreaOutput.getText());
            writer.close();
            Util.addToLog(textAreaLog, "Output saved");
        }
    }

    public void settingsSave(ActionEvent actionEvent) throws ConfigurationException {
        App.propertiesHandle.setProperty("player-lvl", textFieldPlayerLevel.getText());
        App.propertiesHandle.setProperty("iterations", textFieldIterations.getText());
        App.propertiesHandle.save();
        Util.addToLog(textAreaLog, "Settings saved");
    }
}

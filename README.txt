Crystark sim GUI

1. Important notice
This app :
- does not need to enter you EK login / password
- does not contain any viruses, keyloggers etc. I swear :)

2. Prerequisites and launching
- You need to have installed Java Runtime Enviroment - latest from the 8 branch . Download available on : http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html
- You need to make sure that your system is using this verion of java because you can have multiple versions installed

You have two files besides that readme. One .exe - for Windows, one jar for Linux / Mac. Windows users - doubleclick exe file. Linux/Mac users try to doubleclick the jar file, and if this doesn't work enter java -jar crygui-0.1.jar
On the first run app will download the crystark sim and everytime it's updated it'll pull the update by itself.


